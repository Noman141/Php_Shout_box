<?php

include('class/Shout.php');
$shout  = new Shout();

?>
<!DOCTYPE html>
<html>
<head>
	<title>Basic shout Box</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
   <div  class="wrapper clr">
   	  <header class="headoption clr">
   	  	 <h2>Basic Shout System With PHP OOP & MYSQLi</h2>
   	  </header>
	   <section class="content clr">
	   	 <div class="box">
	   	 	<ul>
	   	 	<?php 
	   	 	    
	   	 	  $getData = $shout->getAllData();
	   	 	  if ($getData){
	   	 	      while ($data = $getData->fetch_assoc()){

	   	 	?>	
	   	 		<li><span><?php echo $data['time'];?></span><b><span><?php echo $data['name'];?></b><span><?php echo $data['body'];?></li>
	   	 	<?php } }?>
	   	 	</ul>
	   	 </div>

	   	<div class="shoutbox">
            <?php
              if ($_SERVER["REQUEST_METHOD"] == "POST"){
                  $shoutdata = $shout->setAllData($_POST);
              }

            ?>

	   		<form action="" method="post">
	   			<table>
	   				<tr>
	   					<td>Name</td>
	   					<td>:</td>
	   					<td><input type="text" name="name" placeholder="Enter Your Name" ></td>
	   				</tr>

	   				<tr>
	   					<td>Body</td>
	   					<td>:</td>
	   					<td><input type="text" name="body" placeholder="Enter Your Text" ></td>
	   				</tr>

	   				<tr>
	   					<td></td>
	   					<td></td>
	   					<td><input type="submit"  value="Shout It"></td>
	   				</tr>
	   			</table>
	   		</form>
	   	</div>
	   </section>
	   <footer class="footsection">
	   	<h2>&copy; CopyRight Goes To Noman</h2>
	   </footer>
   </div>
</body>
</html>